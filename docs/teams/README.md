# 📕Documentation: teams


## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **OrganizationalRole** : A Social Role, recognized by the Organization, assigned to Agents when they are hired, included in a team, allocated or participating in activities.
* **TeamMembership** : The allocation of a Team Member to play an Organizational Role in a Team is made through the social relation Team Membership
* **Person** : A human Physical Agent
* **Team** : A Team can be related to a Project.
* **Organization** : A Social Agent involving people and other agents and facilities with an arrangement of responsibilities, authorities and relationships
* **Project** : A Social Object as a temporary endeavor undertaken to create a unique product, service, or result.

## ✒️ Mapping Rules
### OrganizationalRole
A Social Role, recognized by the Organization, assigned to Agents when they are hired, included in a team, allocated or participating in activities.

#### From organizationalrole to OrganizationalRole

* *name* **equal** *name*
* *description* **equal** *description*
* *external_id* **equal** *externalId*
* *internal_id* **equal** *internalId*




### TeamMembership
The allocation of a Team Member to play an Organizational Role in a Team is made through the social relation Team Membership

#### From teammembership to TeamMembership

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### Person
A human Physical Agent

#### From person to Person

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.email* **equal** *email*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### Team
A Team can be related to a Project.

#### From team to Team

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### Organization
A Social Agent involving people and other agents and facilities with an arrangement of responsibilities, authorities and relationships

#### From organization to Organization

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




### Project
A Social Object as a temporary endeavor undertaken to create a unique product, service, or result.

#### From project to Project

* *payload.after.name* **equal** *name*
* *payload.after.description* **equal** *description*
* *payload.after.start_date* **equal** *startDate*
* *payload.after.end_date* **equal** *endDate*
* *payload.after.external_id* **equal** *externalId*
* *payload.after.internal_id* **equal** *internalId*




