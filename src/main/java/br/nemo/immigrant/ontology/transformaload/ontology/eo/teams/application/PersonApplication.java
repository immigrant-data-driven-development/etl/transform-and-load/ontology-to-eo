package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationalRoleExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.PersonExceptionNotFound;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;

@Component
@Transactional
public class PersonApplication {

    @Autowired
    private PersonRepository repository;

    public Person create(Person instance) {
        return this.repository.save(instance);
    }


    public Person retrieveByInternalID(String internalID) throws PersonExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new PersonExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private Person createInstance(IDProjection projection){
        return  Person.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}