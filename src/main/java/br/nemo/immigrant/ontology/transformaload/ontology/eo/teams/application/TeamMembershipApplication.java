package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;


import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.models.OrganizationalRole;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamMembershipRepository;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.*;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.ApplicationAbstract;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.JsonUtil;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.DataSearch;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

import org.bson.Document;
@Component
@Transactional
public class TeamMembershipApplication extends ApplicationAbstract {

    @Autowired
    private TeamMembershipRepository repository;

    @Autowired
    private OrganizationalRoleApplication organizationalRoleApplication;

    @Autowired
    private PersonApplication personApplication;

    @Autowired
    private TeamApplication teamApplication;

    public TeamMembership create(TeamMembership instance) {
        return this.repository.save(instance);
    }

    public TeamMembership create(TeamMembership instance, String payload) throws  Exception, MongoNotFound, OrganizationalRoleExceptionNotFound, PersonExceptionNotFound, TeamExceptionNotFound {

        DataSearch organizationalRoleDataSearch = JsonUtil.retrieve(payload,"organizationalrole","organizationalrole_id");
        DataSearch personDataSearch = JsonUtil.retrieve(payload,"person","person_id");
        DataSearch teamDataSearch = JsonUtil.retrieve(payload,"team","team_id");


        if (organizationalRoleDataSearch.getElementValue() !=null && organizationalRoleDataSearch.getElementValue() != 0){
            Document document = this.retrieveDocument(organizationalRoleDataSearch);
            String internalID = JsonUtil.retrieveInternalID(document.toJson());
            OrganizationalRole organizationalRole = this.organizationalRoleApplication.retrieveByInternalID(internalID);
            instance.setOrganizationalrole(organizationalRole);

        }

        if (personDataSearch.getElementValue() !=null && personDataSearch.getElementValue() != 0){
            Document document = this.retrieveDocument(personDataSearch);
            String internalID = JsonUtil.retrieveInternalID(document.toJson());
            Person person = this.personApplication.retrieveByInternalID(internalID);
            instance.setPerson(person);

        }

        if (teamDataSearch.getElementValue() !=null && teamDataSearch.getElementValue() != 0){
            Document document = this.retrieveDocument(teamDataSearch);
            String internalID = JsonUtil.retrieveInternalID(document.toJson());
            Team team = this.teamApplication.retrieveByInternalID(internalID);
            instance.setTeam(team);

        }

        return this.create(instance);
    }

     public TeamMembership retrieveByInternalID(String internalID) throws TeamMembershiptExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new TeamMembershiptExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private TeamMembership createInstance(IDProjection projection){
        return  TeamMembership.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}