
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.*;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.TeamMembership2EOTeamMembershipFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.TeamMembership2EOTeamMembershipMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class TeamMembership2EOTeamMembershipListener {


    @Autowired
    private TeamMembership2EOTeamMembershipFilter filter;

    @Autowired
    private TeamMembership2EOTeamMembershipMapper mapper;

    @Autowired
    private TeamMembershipService service;

    private final KafkaTemplate<String, String> kafkaTemplate;


    @KafkaListener(topicPattern = "ontology.*.teammembership", groupId = "teammembership2eoteammembership-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{
 
            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (OrganizationExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.teammembership", payload.value());

        }
        catch (PersonExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.teammembership", payload.value());
        }

        catch (TeamExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.teammembership", payload.value());

        }
        catch (ProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.teammembership", payload.value());
        }
        catch (MongoNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.teammembership", payload.value());
        }
        catch (Exception e ){
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.teammembership", groupId = "teammembershipEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }

}
