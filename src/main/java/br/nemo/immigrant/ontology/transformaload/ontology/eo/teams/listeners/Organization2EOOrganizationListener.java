
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services.OrganizationService;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.Organization2EOOrganizationFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.Organization2EOOrganizationMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class Organization2EOOrganizationListener {


    @Autowired
    private Organization2EOOrganizationFilter filter;

    @Autowired
    private Organization2EOOrganizationMapper mapper;

    @Autowired
    private OrganizationService service;


    @KafkaListener(topicPattern = "ontology.*.organization", groupId = "organization2eoorganization-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.organization", groupId = "organizationEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }


}
