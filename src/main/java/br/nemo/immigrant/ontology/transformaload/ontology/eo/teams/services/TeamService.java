
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application.TeamApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.Mapper;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class TeamService {


    @Autowired
    private TeamApplication application;


    public void process(ConsumerRecord<String, String> payload,Mapper<Team> mapper) throws OrganizationExceptionNotFound, TeamExceptionNotFound, ProjectExceptionNotFound, MongoNotFound, Exception{

        Team instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists) {
            this.application.create(instance, payload.value());
        }

    }



}
