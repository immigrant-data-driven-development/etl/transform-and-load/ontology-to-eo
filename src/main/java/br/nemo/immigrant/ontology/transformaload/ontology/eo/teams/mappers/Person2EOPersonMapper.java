    package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers;

    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.Mapper;

    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class Person2EOPersonMapper  implements Mapper <Person>{

    public Person map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("payload").path("after").path("name").asText();

        String description = rootNode.path("payload").path("after").path("description").asText();

        String startdate = rootNode.path("payload").path("after").path("start_date").asText();

        String enddate = rootNode.path("payload").path("after").path("end_date").asText();

        String email = rootNode.path("payload").path("after").path("email").asText();

        String externalid = rootNode.path("payload").path("after").path("external_id").asText();

        String internalid = rootNode.path("payload").path("after").path("internal_id").asText();


        return Person.builder().name(name).
                                description(description).
                                //startDate(startdate).
                                //endDate(enddate).
                                email(email).
                                //externalId(externalid).
                                internalId(internalid).build();
    }
}
