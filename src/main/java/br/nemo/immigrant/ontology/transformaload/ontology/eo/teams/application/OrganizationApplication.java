package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationRepository;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;

@Component
@Transactional
public class OrganizationApplication {

    @Autowired
    private OrganizationRepository repository;

    public Organization create(Organization organization) {
        return this.repository.save(organization);
    }


    public Organization retrieveByInternalID(String internalID) throws OrganizationExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new OrganizationExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private Organization createInstance(IDProjection projection){
        return  Organization.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}