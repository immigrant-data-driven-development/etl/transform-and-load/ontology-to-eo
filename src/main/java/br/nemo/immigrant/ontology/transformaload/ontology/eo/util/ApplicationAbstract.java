package br.nemo.immigrant.ontology.transformaload.ontology.eo.util;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.DataSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.bson.Document;
import java.util.List;

public abstract class ApplicationAbstract {

    @Autowired
    private MongoApplication mongoApplication;

    protected Document retrieveDocument(DataSearch data) throws MongoNotFound {
        List<Document> documents = mongoApplication.find(data.getTable(),
                data.getElementValue(),
                data.getDatabase());
        return documents.get(0);
    }
}
