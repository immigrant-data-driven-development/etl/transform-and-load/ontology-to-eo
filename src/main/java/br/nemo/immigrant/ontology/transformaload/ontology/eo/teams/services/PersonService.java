
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application.PersonApplication;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.Mapper;



import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class PersonService {

    @Autowired
    private PersonApplication application;



    public void process(ConsumerRecord<String, String> payload,Mapper<Person> mapper)  throws Exception{

        Person instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create (instance);
        }
    }


}
