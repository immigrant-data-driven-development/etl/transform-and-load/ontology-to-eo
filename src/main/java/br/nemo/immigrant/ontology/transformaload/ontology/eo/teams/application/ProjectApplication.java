package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.ProjectRepository;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.ApplicationAbstract;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.JsonUtil;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.DataSearch;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.bson.Document;
@Component
@Transactional
public class ProjectApplication extends ApplicationAbstract {

    @Autowired
    private ProjectRepository repository;

    @Autowired
    private OrganizationApplication organizationApplication;

    public Project create(Project instance, String payload) throws  Exception, MongoNotFound, OrganizationExceptionNotFound, ProjectExceptionNotFound {

        DataSearch organizationDataSearch = JsonUtil.retrieve(payload,"organization","organization_id");
        DataSearch projectDataSearch = JsonUtil.retrieve(payload,"project","project_id");


        if (organizationDataSearch.getElementValue() !=null && organizationDataSearch.getElementValue() != 0){
            Document organizationDocument = this.retrieveDocument(organizationDataSearch);
            String internalID = JsonUtil.retrieveInternalID(organizationDocument.toJson());
            Organization organization = this.organizationApplication.retrieveByInternalID(internalID);
            instance.setOrganization(organization);

        }

        if (projectDataSearch.getElementValue() !=null && projectDataSearch.getElementValue() != 0 ){
            Document projectDocument = this.retrieveDocument(projectDataSearch);
            String internalID = JsonUtil.retrieveInternalID(projectDocument.toJson());
            Project project = this.retrieveByInternalID(internalID);
            instance.setProject(project);
        }


        return this.create(instance);
    }


    public Project create(Project project) {
        return this.repository.save(project);
    }


    public Project retrieveByInternalID(String internalID) throws ProjectExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new ProjectExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private Project createInstance(IDProjection projection){
        return  Project.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}