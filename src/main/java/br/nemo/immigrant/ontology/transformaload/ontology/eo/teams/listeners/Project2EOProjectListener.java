
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.Project2EOProjectFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.Project2EOProjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class Project2EOProjectListener {


    @Autowired
    private Project2EOProjectFilter filter;

    @Autowired
    private Project2EOProjectMapper mapper;

    @Autowired
    private ProjectService service;

    private final KafkaTemplate<String, String> kafkaTemplate;
    @KafkaListener(topicPattern  = "ontology.*.project", groupId = "project2eoproject-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }catch (OrganizationExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.project", payload.value());

        }
        catch (ProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.project", payload.value());
        }
        catch (MongoNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.project", payload.value());
        }
        catch (Exception e ){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.project", groupId = "projectEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }

}
