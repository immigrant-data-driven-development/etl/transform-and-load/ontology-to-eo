
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services.OrganizationalRoleService;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.OrganizationalRole2EOOrganizationalRoleFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.OrganizationalRole2EOOrganizationalRoleMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrganizationalRole2EOOrganizationalRoleListener {


    @Autowired
    private OrganizationalRole2EOOrganizationalRoleFilter filter;

    @Autowired
    private OrganizationalRole2EOOrganizationalRoleMapper mapper;

    @Autowired
    private OrganizationalRoleService service;


    @KafkaListener(topicPattern = "ontology.*.organizationalrole", groupId = "organizationalrole2eoorganizationalrole-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.organizationalrole", groupId = "organizationalroleEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }

}
