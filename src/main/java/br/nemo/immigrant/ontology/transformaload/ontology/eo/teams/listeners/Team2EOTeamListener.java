
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.Team2EOTeamFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.Team2EOTeamMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class Team2EOTeamListener {


    @Autowired
    private Team2EOTeamFilter filter;

    @Autowired
    private Team2EOTeamMapper mapper;

    @Autowired
    private TeamService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    @KafkaListener(topicPattern = "ontology.*.team", groupId = "team2eoteam-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }
        catch (OrganizationExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.team", payload.value());

        }
        catch (TeamExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.team", payload.value());

        }
        catch (ProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.team", payload.value());
        }
        catch (MongoNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.eo.team", payload.value());
        }
        catch (Exception e ){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.team", groupId = "teamEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }


}
