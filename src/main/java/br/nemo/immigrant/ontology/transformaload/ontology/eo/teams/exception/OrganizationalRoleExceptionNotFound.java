package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class OrganizationalRoleExceptionNotFound extends RuntimeException{

    public OrganizationalRoleExceptionNotFound(String message) {
        super(message);
    }
}
