package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class TeamExceptionNotFound extends RuntimeException{

    public TeamExceptionNotFound(String message) {
        super(message);
    }
}
