    package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers;

    import br.nemo.immigrant.ontology.entity.eo.teams.models.OrganizationalRole;
    import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.Mapper;

    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class OrganizationalRole2EOOrganizationalRoleMapper  implements Mapper <OrganizationalRole>{

    public OrganizationalRole map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("name").asText();

        String description = rootNode.path("description").asText();

        String externalid = rootNode.path("external_id").asText();

        String internalid = rootNode.path("internal_id").asText();


        return OrganizationalRole.builder().name(name).
                                            description(description).
                                            //externalId(externalid).
                                            internalId(internalid).build();
    }
}
