package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class OrganizationExceptionNotFound extends RuntimeException{

    public OrganizationExceptionNotFound(String message) {
        super(message);
    }
}
