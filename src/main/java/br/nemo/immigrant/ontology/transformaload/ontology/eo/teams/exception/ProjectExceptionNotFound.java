package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class ProjectExceptionNotFound extends RuntimeException{

    public ProjectExceptionNotFound(String message) {
        super(message);
    }
}
