
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationalRoleExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.Mapper;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application.TeamMembershipApplication;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class TeamMembershipService {

    
    @Autowired
    private TeamMembershipApplication application;



    public void process(ConsumerRecord<String, String> payload,Mapper<TeamMembership> mapper) throws MongoNotFound, OrganizationalRoleExceptionNotFound, PersonExceptionNotFound, TeamExceptionNotFound, Exception {

       TeamMembership instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.create(instance, payload.value());
        }
    }


}
