package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.listeners.mongo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrganizationInputListener {


    @Autowired
    private MongoTemplate mongoTemplate;

    @KafkaListener(topicPattern = "ontology.*.organization", groupId = "inputorganizationEO-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{    

            String data = payload.value();
            Document doc = Document.parse (data);
            mongoTemplate.insert(doc, "input");

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}
