package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.MongoNotFound;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Organization;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.ApplicationAbstract;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.util.mongo.DataSearch;
import org.bson.Document;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;

@Component
@Slf4j
@Transactional
public class TeamApplication extends ApplicationAbstract {

    @Autowired
    private TeamRepository repository;

    @Autowired
    private OrganizationApplication organizationApplication;

    @Autowired
    private ProjectApplication projectApplication;



    public Team create(Team instance, String payload) throws  Exception, MongoNotFound, OrganizationExceptionNotFound, TeamExceptionNotFound, ProjectExceptionNotFound {

        DataSearch organizationDataSearch = JsonUtil.retrieve(payload,"organization","organization_id");
        DataSearch projectDataSearch = JsonUtil.retrieve(payload,"project","project_id");
        DataSearch teamDataSearch = JsonUtil.retrieve(payload,"team","team_id");


        if (organizationDataSearch.getElementValue() !=null && organizationDataSearch.getElementValue() != 0){
            Document organizationDocument = this.retrieveDocument(organizationDataSearch);
            String internalID = JsonUtil.retrieveInternalID(organizationDocument.toJson());
            Organization organization = this.organizationApplication.retrieveByInternalID(internalID);
            instance.setOrganization(organization);

        }

        if (projectDataSearch.getElementValue() !=null && projectDataSearch.getElementValue() != 0 ){
            Document projectDocument = this.retrieveDocument(projectDataSearch);
            String internalID = JsonUtil.retrieveInternalID(projectDocument.toJson());
            Project project = this.projectApplication.retrieveByInternalID(internalID);
            instance.setProject(project);
        }

        if (teamDataSearch.getElementValue() !=null && teamDataSearch.getElementValue() != 0 ){
            Document teamDocument = this.retrieveDocument(teamDataSearch);
            String internalID = JsonUtil.retrieveInternalID(teamDocument.toJson());
            Team otherTeam = this.retrieveByInternalID(internalID);
            instance.setTeam(otherTeam);

        }
        return this.repository.save(instance);
    }



    public Team retrieveByInternalID(String internalID) throws TeamExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private Team createInstance(IDProjection projection){
        return  Team.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }


}