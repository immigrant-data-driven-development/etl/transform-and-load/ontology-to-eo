package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.application;


import br.nemo.immigrant.ontology.entity.eo.teams.models.OrganizationalRole;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.OrganizationalRoleRepository;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception.OrganizationalRoleExceptionNotFound;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

@Component
@Transactional
public class OrganizationalRoleApplication {

    @Autowired
    private OrganizationalRoleRepository repository;

    public OrganizationalRole create(OrganizationalRole organizationRole) {
        return this.repository.save(organizationRole);
    }

    public OrganizationalRole retrieveByInternalID(String internalID) throws OrganizationalRoleExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new OrganizationalRoleExceptionNotFound(internalID));

        return createInstance(projection);
    }

    private OrganizationalRole createInstance(IDProjection projection){
        return  OrganizationalRole.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}