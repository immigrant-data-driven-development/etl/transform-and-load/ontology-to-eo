package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class TeamMembershiptExceptionNotFound extends RuntimeException{

    public TeamMembershiptExceptionNotFound(String message) {
        super(message);
    }
}
