package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.exception;

public class PersonExceptionNotFound extends RuntimeException{

    public PersonExceptionNotFound(String message) {
        super(message);
    }
}
