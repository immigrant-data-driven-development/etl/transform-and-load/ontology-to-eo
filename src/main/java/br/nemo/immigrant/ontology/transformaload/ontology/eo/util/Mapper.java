package br.nemo.immigrant.ontology.transformaload.ontology.eo.util;

public interface Mapper <T> {
  public T map (String element) throws Exception;
}
