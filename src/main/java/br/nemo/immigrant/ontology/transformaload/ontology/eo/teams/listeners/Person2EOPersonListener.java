
package br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services;

import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.filters.Person2EOPersonFilter;
import br.nemo.immigrant.ontology.transformaload.ontology.eo.teams.mappers.Person2EOPersonMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class Person2EOPersonListener {


    @Autowired
    private Person2EOPersonFilter filter;

    @Autowired
    private Person2EOPersonMapper mapper;

    @Autowired
    private PersonService service;


    @KafkaListener(topicPattern = "ontology.*.person", groupId = "person2eoperson-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.eo.person", groupId = "personEONotthing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }

}
